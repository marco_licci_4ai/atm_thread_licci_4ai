package com.marcolicci;

public class Main {

    public static void main(String[] args) {
        Account account = new Account(1000);

        new Thread(new ATM(account, 800)).start();
        new Thread(new ATM(account, 750)).start();

        account = new Account(1000);
        new Thread(new ATMSync(account, 800)).start();
        new Thread(new ATMSync(account, 750)).start();
    }
}
