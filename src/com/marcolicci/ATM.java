package com.marcolicci;

/**
 * Created by marco on 07/04/2017.
 */
public class ATM extends Thread {
    private Account account;
    private final int amount;

    public ATM(Account account, int amount) {
        this.account = account;
        this.amount = amount;
    }

    @Override
    public void run() {
        withdraw();
    }

    protected void withdraw(){
        int newBalance = account.getMoney() - amount;
        if(newBalance > 0){
            account.setMoney(newBalance);
            System.out.println("Withdrawal successful.");
            System.out.println("Current balance " + newBalance);
            System.out.println();
        } else {
            System.out.println("Not enough funds!");
            System.out.println("Current balance: " + account.getMoney());
            System.out.println();
        }
    }
}
