package com.marcolicci;

/**
 * Created by marco on 11/04/2017.
 */
public class ATMSync extends ATM {
    public ATMSync(Account account, int amount) {
        super(account, amount);
    }

    @Override
    protected synchronized void withdraw() {
        super.withdraw();
    }
}
